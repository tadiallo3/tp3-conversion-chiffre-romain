package com.uca;

import org.junit.jupiter.api.Test;
import java.util.concurrent.Callable;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Tests {
	
	@Test
	public void testConverter(){
   
		assertThat(RomanConverter.getRomanFromNumber(4), equalTo("IV"));
        assertThat(RomanConverter.getRomanFromNumber(10), equalTo("X"));
        assertThat(RomanConverter.getRomanFromNumber(50), equalTo("L"));
		assertThat(exceptionOf(() -> RomanConverter.getRomanFromNumber(-2)), instanceOf(IllegalArgumentException.class));
        assertThat(RomanConverter.getNumberFromRoman("V"), equalTo(5));
        assertThat(RomanConverter.getNumberFromRoman("M"), equalTo(1000));
        assertThat(RomanConverter.getNumberFromRoman("D"), equalTo(500));
 
	}

	//TODO : les autres tests  
    public void romanClasstest(){
        RomanNumber number1 = new RomanNumber(10,"X"); 
         assertEquals(10, number1.getValue());
         assertEquals("X", number1.getRoman());

         RomanNumber number2 = new RomanNumber("V"); 
         assertEquals(number2, number2.getValue());
         assertEquals(number2, number2.getRoman());

         RomanNumber number3 = new RomanNumber(10);
         assertEquals(number3, number3.getRoman()); 

         assertEquals( 10.0, number1.doubleValue());
       // assert
    }

	


    //Help you to handle exception. :-)
    public static Throwable exceptionOf(Callable<?> callable) {
        try {
            callable.call();
            return null;
        } catch (Throwable t) {
            return t;
        }
    }
}
